package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 * Problema
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaMSF4JServerCodegen", date = "2018-11-03T03:16:57.879Z")
public class Problema   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("dataInclusao")
  private Date dataInclusao = null;

  @JsonProperty("latitude")
  private String latitude = null;

  @JsonProperty("longitude")
  private String longitude = null;

  /**
   * 
   */
  public enum TipoEnum {
    VIAS("vias"),
    
    ILUMINA_O("iluminação"),
    
    SEGURAN_A("segurança"),
    
    AGUASESGOTO("aguasEsgoto"),
    
    AMBIENTAL("ambiental");

    private String value;

    TipoEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TipoEnum fromValue(String text) {
      for (TipoEnum b : TipoEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("tipo")
  private TipoEnum tipo = null;

  @JsonProperty("criticidade")
  private String criticidade = null;

  @JsonProperty("informacoesAdicionais")
  private String informacoesAdicionais = null;

  @JsonProperty("chaveDoArquivo")
  private String chaveDoArquivo = null;

  @JsonProperty("foto")
  private String foto = null;

  public Problema id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Problema email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Problema dataInclusao(Date dataInclusao) {
    this.dataInclusao = dataInclusao;
    return this;
  }

   /**
   * Get dataInclusao
   * @return dataInclusao
  **/
  @ApiModelProperty(value = "")
  public Date getDataInclusao() {
    return dataInclusao;
  }

  public void setDataInclusao(Date dataInclusao) {
    this.dataInclusao = dataInclusao;
  }

  public Problema latitude(String latitude) {
    this.latitude = latitude;
    return this;
  }

   /**
   * Get latitude
   * @return latitude
  **/
  @ApiModelProperty(value = "")
  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public Problema longitude(String longitude) {
    this.longitude = longitude;
    return this;
  }

   /**
   * Get longitude
   * @return longitude
  **/
  @ApiModelProperty(value = "")
  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public Problema tipo(TipoEnum tipo) {
    this.tipo = tipo;
    return this;
  }

   /**
   * 
   * @return tipo
  **/
  @ApiModelProperty(value = "")
  public TipoEnum getTipo() {
    return tipo;
  }

  public void setTipo(TipoEnum tipo) {
    this.tipo = tipo;
  }

  public Problema criticidade(String criticidade) {
    this.criticidade = criticidade;
    return this;
  }

   /**
   * Get criticidade
   * @return criticidade
  **/
  @ApiModelProperty(value = "")
  public String getCriticidade() {
    return criticidade;
  }

  public void setCriticidade(String criticidade) {
    this.criticidade = criticidade;
  }

  public Problema informacoesAdicionais(String informacoesAdicionais) {
    this.informacoesAdicionais = informacoesAdicionais;
    return this;
  }

   /**
   * Get informacoesAdicionais
   * @return informacoesAdicionais
  **/
  @ApiModelProperty(value = "")
  public String getInformacoesAdicionais() {
    return informacoesAdicionais;
  }

  public void setInformacoesAdicionais(String informacoesAdicionais) {
    this.informacoesAdicionais = informacoesAdicionais;
  }

  public Problema chaveDoArquivo(String chaveDoArquivo) {
    this.chaveDoArquivo = chaveDoArquivo;
    return this;
  }

   /**
   * Get chaveDoArquivo
   * @return chaveDoArquivo
  **/
  @ApiModelProperty(value = "")
  public String getChaveDoArquivo() {
    return chaveDoArquivo;
  }

  public void setChaveDoArquivo(String chaveDoArquivo) {
    this.chaveDoArquivo = chaveDoArquivo;
  }

  public Problema foto(String foto) {
    this.foto = foto;
    return this;
  }

   /**
   * Get foto
   * @return foto
  **/
  @ApiModelProperty(value = "")
  public String getFoto() {
    return foto;
  }

  public void setFoto(String foto) {
    this.foto = foto;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Problema problema = (Problema) o;
    return Objects.equals(this.id, problema.id) &&
        Objects.equals(this.email, problema.email) &&
        Objects.equals(this.dataInclusao, problema.dataInclusao) &&
        Objects.equals(this.latitude, problema.latitude) &&
        Objects.equals(this.longitude, problema.longitude) &&
        Objects.equals(this.tipo, problema.tipo) &&
        Objects.equals(this.criticidade, problema.criticidade) &&
        Objects.equals(this.informacoesAdicionais, problema.informacoesAdicionais) &&
        Objects.equals(this.chaveDoArquivo, problema.chaveDoArquivo) &&
        Objects.equals(this.foto, problema.foto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, dataInclusao, latitude, longitude, tipo, criticidade, informacoesAdicionais, chaveDoArquivo, foto);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Problema {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    dataInclusao: ").append(toIndentedString(dataInclusao)).append("\n");
    sb.append("    latitude: ").append(toIndentedString(latitude)).append("\n");
    sb.append("    longitude: ").append(toIndentedString(longitude)).append("\n");
    sb.append("    tipo: ").append(toIndentedString(tipo)).append("\n");
    sb.append("    criticidade: ").append(toIndentedString(criticidade)).append("\n");
    sb.append("    informacoesAdicionais: ").append(toIndentedString(informacoesAdicionais)).append("\n");
    sb.append("    chaveDoArquivo: ").append(toIndentedString(chaveDoArquivo)).append("\n");
    sb.append("    foto: ").append(toIndentedString(foto)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

