package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.DefaultApiService;
import io.swagger.api.factories.DefaultApiServiceFactory;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import io.swagger.model.Problema;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import org.wso2.msf4j.formparam.FormDataParam;
import org.wso2.msf4j.formparam.FileInfo;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/")


@io.swagger.annotations.Api(description = "the  API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaMSF4JServerCodegen", date = "2018-11-03T03:16:57.879Z")
public class DefaultApi  {
   private final DefaultApiService delegate = DefaultApiServiceFactory.getDefaultApi();

    @POST
    
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/xml", "application/json" })
    @io.swagger.annotations.ApiOperation(value = "adiciona um novo item de fiscalização", notes = "", response = Void.class, tags={ "dados", })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "Dados inseridos com sucesso", response = Void.class),
        
        @io.swagger.annotations.ApiResponse(code = 400, message = "Dados inválidos", response = Void.class),
        
        @io.swagger.annotations.ApiResponse(code = 500, message = "Ocorreu um erro ao tentar inserir os dados", response = Void.class) })
    public Response adicionarDados(@ApiParam(value = "Informações de Fiscalização" ,required=true) Problema body
)
    throws NotFoundException {
        return delegate.adicionarDados(body);
    }
}
