package br.com.hackathon.security;

import java.security.Principal;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

@Component
public class SessionUtils {

    @Autowired
    private SessionRegistry sessionRegistry;

    public void getAllSessionsDetails(){
        ArrayList<Principal> p = new ArrayList<>();
        System.out.println(RequestContextHolder.currentRequestAttributes().getSessionId());
        for(Object principal : sessionRegistry.getAllPrincipals()){
            p.add((Principal)principal);
        }

    }
}
