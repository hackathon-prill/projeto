package br.com.hackathon.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import br.com.hackathon.cdi.CDIServiceLocator;

public class MySimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	protected Log logger = LogFactory.getLog(this.getClass());
	
	private RedirectStrategy redirectStrategy;
	//private Seguranca seguranca;
	
	public MySimpleUrlAuthenticationSuccessHandler() {
		redirectStrategy = CDIServiceLocator.getBean(DefaultRedirectStrategy.class);
		//seguranca = CDIServiceLocator.getBean(Seguranca.class);
	}
	
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		handle(request, response, authentication);
        clearAuthenticationAttributes(request);
	}
	
	private void handle(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException {
		String targetUrl = determineTargetUrl(authentication);
 
		if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}
	
	private String determineTargetUrl(Authentication authentication) {
		/*if(seguranca.hasAnyTipoUsuario(TipoUsuario.ADMINISTRADOR)) {
			return Link.DASHBOARD_ADM.getLinkParcial();
		} else if(seguranca.hasAnyTipoUsuario(TipoUsuario.USUARIO)) {
			return Link.DASHBOARD.getLinkParcial();
		} else {
			return null;
		}*/
		return null;
	}
	
	protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
	
	
	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}