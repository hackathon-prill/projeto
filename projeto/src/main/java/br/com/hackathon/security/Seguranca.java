package br.com.hackathon.security;

import java.io.Serializable;

import javax.inject.Inject;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.hackathon.entity.Usuario;
import br.com.hackathon.negocio.UsuarioRN;

public class Seguranca implements Serializable {

	private static final long serialVersionUID = 8159350085080464823L;
	
	@Inject
	private UsuarioRN usuarioRN;
	
	public Usuario getUsuarioLogado() {
		Usuario usuario = null;
		UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken)
				SecurityContextHolder.getContext().getAuthentication();

		if (auth != null && auth.getPrincipal() != null) {
			usuario = usuarioRN.findById((Usuario) auth.getPrincipal());
		}
		
		return usuario;
	}
}
