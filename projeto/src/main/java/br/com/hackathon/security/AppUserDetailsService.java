package br.com.hackathon.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.hackathon.cdi.CDIServiceLocator;
import br.com.hackathon.lib.entities.Usuario;
import br.com.hackathon.negocio.UsuarioRN;

public class AppUserDetailsService implements UserDetailsService {
	
	private UsuarioRN usuarioRN;

	public AppUserDetailsService() {
		super();
		usuarioRN = CDIServiceLocator.getBean(UsuarioRN.class);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		if(email.equals("x")) {
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_ADM"));
			Usuario usuario = new Usuario(1, "Rafael", "Bruno", "rafael.rosa@prill.com.br");
			return new UsuarioSistema(usuario, authorities);
		}
		return null;
		
		//Usuario usuario = usuarioRN.findAtivoByEmail(email);
		
		/*if(usuario != null && !hasAnyTipoUsuario(usuario, TipoUsuario.ADMINISTRADOR)) {
			MensalidadeRN mensalidadeRN = CDIServiceLocator.getBean(MensalidadeRN.class);
			if(!mensalidadeRN.habilitarLogin(usuario)) {
				throw new BadCredentialsException("erro_bloqueado_falta_pagamento");
			}
		} else*//* if (usuario == null) {
			throw new BadCredentialsException("erro_usuario_ou_senha_invalido");
		}
		
		return null;*/ //new UsuarioSistema(usuario, getGrupos(usuario));
	}
}