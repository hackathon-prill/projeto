package br.com.hackathon.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired 
    private UserDetailsService userDetailService;  
	
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(new Md5PasswordEncoder());
    }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	JsfLoginUrlAuthenticationEntryPoint jsfLoginEntry = new JsfLoginUrlAuthenticationEntryPoint();
		jsfLoginEntry.setLoginFormUrl("/Login.xhtml");
		jsfLoginEntry.setRedirectStrategy(new JsfRedirectStrategy());
		
		JsfAccessDeniedHandler jsfDeniedEntry = new JsfAccessDeniedHandler();
		jsfDeniedEntry.setLoginPath("/AcessoNegado.xhtml");
		jsfDeniedEntry.setContextRelative(true);
		
    	 http.csrf().disable()
    	 	.headers().frameOptions().sameOrigin()
    	 	.and()
    	 	.logout()
				.logoutSuccessUrl("/Login.xhtml")
				.and()
			.authorizeRequests()  
				.antMatchers("/Login.xhtml", "/javax.faces.resource/**").permitAll()
				//.antMatchers("/restrito/**").authenticated()
				/*.antMatchers(Link.ALTERAR_SENHA.getLinkParcial())
					.hasAnyRole(PaginaRoles.ALTERAR_SENHA.toString())*/
				.and()
			.formLogin()
				.loginPage("/Login.xhtml")
				.failureUrl("/Login.xhtml")
				.successHandler(new MySimpleUrlAuthenticationSuccessHandler())
				.and()
				
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")) //link de logout
				.and()
				
			.exceptionHandling()
				.accessDeniedPage("/AcessoNegado.xhtml")
				.authenticationEntryPoint(jsfLoginEntry)
				.accessDeniedHandler(jsfDeniedEntry)
				.and();
			/*.requiresChannel()
		         .anyRequest()
		         .requiresSecure()
		         .and()
			.sessionManagement()
		    	.maximumSessions(-1)
		    	.sessionRegistry(sessionRegistry());*/
    	 
    	 //link reativo
    }

    @Bean
    public AppUserDetailsService userDetailsService() {
    	return new AppUserDetailsService();
    }
    
    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
}