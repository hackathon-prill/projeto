package br.com.hackathon.validation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import br.com.hackathon.util.MensagemUtil;

@FacesValidator(value="emailValidator")
public class EmailValidator implements Validator{
	/*
	 * Geral Email formato (RE) que incluem tamb�m dom�nio como co.in, co.uk, com, outlook.com etc
     * 
     * E a regra diz que:
	 * 
	 * Letras mai�sculas e min�sculas em ingl�s (az, AZ)
	 * D�gitos 0 a 9
	 * Personagens ! -benz�ico. -benz�ico. } ~ Caractere.
	 * (Ponto, ponto, ponto final) desde que n�o seja o primeiro ou o �ltimo caractere, e desde que ele n�o apare�a duas ou mais vezes consecutivas.
	 * 
	 * [a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6}
	 */
	
	private static final String EMAIL_PATTERN = "([a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6})?";
	
	private Pattern pattern;
	private Matcher matcher;

	public EmailValidator(){
		  pattern = Pattern.compile(EMAIL_PATTERN);
	}

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

		matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
			FacesMessage msg =
				new FacesMessage(MensagemUtil.getMensagem("validator_Email"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}