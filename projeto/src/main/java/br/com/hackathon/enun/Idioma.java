package br.com.hackathon.enun;

import java.util.Locale;

public enum Idioma {
	PORTUGUES("Portugu�s", new Locale("pt", "BR"));
	/*ESPANOL("Espa�ol", new Locale("es", "ES")),
	ENGLISH("Enghish", new Locale("en", "US"));*/
	
	private String descricao;
	private Locale locale;
	
	private Idioma(String descricao, Locale locale) {
		this.descricao = descricao;
		this.locale = locale;
	}

	@Override
	public String toString() {
		return descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public Locale getLocale() {
		return locale;
	}
}
