package br.com.hackathon.util.mail;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import br.com.hackathon.lib.enuns.EmailDados;
import br.com.hackathon.util.MailException;
import br.com.hackathon.util.MensagemUtil;

public class Email {
	protected Session getSessionAdministrador() throws MailException {
		try {
			Properties props = new Properties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.host", "smtp-mail.outlook.com");
	        props.put("mail.smtp.socketFactory.port", "587");
	        props.put("mail.smtp.socketFactory.fallback", "true");
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.port", "587");
	        Session session = Session.getDefaultInstance(props,
	                new javax.mail.Authenticator() {
	                    @Override
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                    	return new PasswordAuthentication(
	                    			EmailDados.EMAIL_SISTEMA.getDescricao(),
	                    			EmailDados.SENHA_SISTEMAS.getDescricao());  
	                    }
	                });
	        session.setDebug(true);
			
			/*Properties props = new Properties();
			props.load(getClass().getResourceAsStream("/mail/mail.properties")); //pega as configurações para enviar o email
			Session session = Session.getDefaultInstance(props, new Authenticator() { //passa os dados do properties e cria o usuário e senha
				protected PasswordAuthentication getPasswordAuthentication()   
	            {
	        		return new PasswordAuthentication(de, password);  
	            }
			}); //passa os dados do properties
*/			return session;
		/*} catch (IOException e){ // IOException - if an error occurred when reading from the input stream.
			throw new UtilException(MensagemUtil.getMensagem("erro_email_leitura_configuracao") + Email.class, e);*/
		} catch (IllegalArgumentException e) {
			throw new MailException(MensagemUtil.getMensagem("erro_email_caracter_invalido", EmailDados.EMAIL_SISTEMA.getDescricao()), e);
		}
	}
}
