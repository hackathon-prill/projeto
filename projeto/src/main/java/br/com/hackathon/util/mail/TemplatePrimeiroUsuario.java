package br.com.hackathon.util.mail;

import java.io.File;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import br.com.hackathon.lib.entities.Usuario;
import br.com.hackathon.lib.enuns.EmailDados;
import br.com.hackathon.lib.enuns.Imagem;
import br.com.hackathon.lib.enuns.Link;
import br.com.hackathon.lib.enuns.Sistema;
import br.com.hackathon.util.MailException;
import br.com.hackathon.util.MensagemUtil;

public class TemplatePrimeiroUsuario implements ITemplate {
	
	private MimeMessage mimeMessage;
	private Usuario usuario;
	private ResourceBundle bundle;
	
	@Override
	public void configurar(Session session, String para, Object object) throws MessagingException {
		//Configura��o do e-mail
				usuario = (Usuario) object;
				
				mimeMessage = new MimeMessage(session);
				mimeMessage.setFrom(EmailDados.EMAIL_SISTEMA.getDescricao());
				mimeMessage.setRecipients(Message.RecipientType.TO, para);
				bundle = MensagemUtil.getBundle(Locale.forLanguageTag("pt"));
				mimeMessage.setSubject(formatar(
								bundle.getString("email_primeiro_usuario_subject"), Sistema.SISTEMA_NOME.getDescricao()));
	}

	@Override
	public void enviarEmail() throws MailException {
		try {
			// Criando corpo da mensagem (com texto e html)
			MimeMultipart mpContent = new MimeMultipart("alternative");
			mpContent.addBodyPart(mbpTextPlain(usuario)); // Adicionando texto puro � raiz
			mpContent.addBodyPart(mbpTextHtml(usuario)); // Adicionando texto html � raiz
			
			// A raiz para agrupar os dois tipos de textos
			MimeBodyPart corpoRaiz = new MimeBodyPart();
			corpoRaiz.setContent(mpContent);
			
			// Criar tipo de mensagem multipart para permitir inclus�o de anexos
			Multipart mpCorpoPrincipal = new MimeMultipart("related");
			mpCorpoPrincipal.addBodyPart(corpoRaiz); // Adiciona a raiz � mensagem
	        
	        // Obt�m a refer�ncia ao arquivo de imagem
	        
	        // Insere na mensagem
	        mpCorpoPrincipal.addBodyPart(mbpImagemInline());
			
			mimeMessage.setContent(mpCorpoPrincipal);
			// Envia a mensagem
			
			Transport.send(mimeMessage);
		} catch (Exception exception){
			throw new MailException(MensagemUtil.getMensagem("erro_email_enviar"), exception);
		}
	}
	
	private MimeBodyPart mbpTextHtml(Usuario usuario) throws MessagingException {
		VelocityEngine ve = inicializarVelocity();
        
        Template t = ve.getTemplate(EmailDados.PRIMEIRO_USUARIO_HTML.getDescricao());
        
        VelocityContext context = new VelocityContext();
        
        bundle = MensagemUtil.getBundle(Locale.forLanguageTag("pt"));
        
        context.put("email_primeiro_usuario_titulo", formatar(bundle.getString("email_primeiro_usuario_titulo"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("email_primeiro_usuario_ola", bundle.getString("email_primeiro_usuario_ola"));
        context.put("email_primeiro_usuario_mensagem1", formatar(bundle.getString("email_primeiro_usuario_mensagem1"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("email_primeiro_usuario_mensagem2", bundle.getString("email_primeiro_usuario_mensagem2"));
        context.put("email_primeiro_usuario_mensagem3", bundle.getString("email_primeiro_usuario_mensagem3"));
        context.put("email_primeiro_usuario_mensagem4", bundle.getString("email_primeiro_usuario_mensagem4"));
        context.put("email_primeiro_usuario_mensagem5", bundle.getString("email_primeiro_usuario_mensagem5"));
        context.put("email_primeiro_usuario_mensagem6", formatar(bundle.getString("email_primeiro_usuario_mensagem6"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("usuario", usuario);
        context.put("link", Link.LINK_WEB.getDescricao() + Link.ATIVAR_CONTA.getDescricao() + usuario.getCodigoHabilitarSenha());
        
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        
		// Adicionando texto html � raiz
		MimeBodyPart mbpTextHtml = new MimeBodyPart();
		mbpTextHtml.setText(writer.toString(), "iso-8859-1", "html");
		return mbpTextHtml;
	}
	
	private MimeBodyPart mbpTextPlain(Usuario usuario) throws MessagingException {
		VelocityEngine ve = inicializarVelocity();
        
        Template t = ve.getTemplate(EmailDados.PRIMEIRO_USUARIO_PLAIN.getDescricao());
        
        VelocityContext context = new VelocityContext();
        
        bundle = MensagemUtil.getBundle(Locale.forLanguageTag("pt"));
        context.put("email_primeiro_usuario_titulo", formatar(bundle.getString("email_primeiro_usuario_titulo"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("email_primeiro_usuario_ola", bundle.getString("email_primeiro_usuario_ola"));
        context.put("email_primeiro_usuario_plain1", formatar(bundle.getString("email_primeiro_usuario_plain1"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("email_primeiro_usuario_plain2", bundle.getString("email_primeiro_usuario_plain2"));
        context.put("email_primeiro_usuario_plain3", bundle.getString("email_primeiro_usuario_plain3"));
        context.put("email_primeiro_usuario_plain4", bundle.getString("email_primeiro_usuario_plain4"));
        context.put("email_primeiro_usuario_plain5", formatar(bundle.getString("email_primeiro_usuario_plain5"), Sistema.SISTEMA_NOME.getDescricao()));
        context.put("usuario", usuario);
        context.put("link", Link.LINK_WEB.getDescricao() + Link.ATIVAR_CONTA.getDescricao() + usuario.getCodigoHabilitarSenha());
        
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        
		// Adicionando texto plain � raiz
		MimeBodyPart mbpTextPlain = new MimeBodyPart();
		mbpTextPlain.setText(writer.toString(), "iso-8859-1", "plain");
		return mbpTextPlain;
	}
	
	// Adicionar imagens ao corpo do e-mail
	private MimeBodyPart mbpImagemInline() throws URISyntaxException, MessagingException {
        File f = new File(this.getClass().getResource(Imagem.LOGO_SISTEMA.getDescricao()).toURI()); //sem rodar no TomCat
        //File img = new File(Thread.currentThread().getContextClassLoader().getResource("imagens/akpm.jpg").toURI()); //Rodando com o TomCat
        
        // Cria a parte de e-mail que ir� armazenar a imagem e adiciona o arquivo
        MimeBodyPart mbpImagemInline = new MimeBodyPart();
        mbpImagemInline.setDataHandler(new DataHandler(new FileDataSource(f)));
        mbpImagemInline.setFileName(f.getName());
        // Define um id que pode ser utilizado no html
        mbpImagemInline.setHeader("Content-ID", "<img1>");
        return mbpImagemInline;
	}

	private VelocityEngine inicializarVelocity() {
		VelocityEngine ve = new VelocityEngine();
	    ve.setProperty("resource.loader", "file");
	    ve.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve.init();
		return ve;
	}
	
	private String formatar(String mensagem, Object... parametros) {
		// aplicamos os parametros aos (parametros da mensagem - {0} .. {1}...)	
		MessageFormat formatter = new MessageFormat(mensagem);
		// pega o primeiro valor do array e coloca no primeiro prm da mensagem....
		return formatter.format(parametros);
	}
}