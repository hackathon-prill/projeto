package br.com.hackathon.util.mail;

import javax.mail.MessagingException;
import javax.mail.Session;

import br.com.hackathon.util.MailException;

public interface ITemplate {
	
	void configurar(Session session, String para, Object object) throws MessagingException;
	void enviarEmail() throws MailException;
}
