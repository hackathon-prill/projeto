package br.com.hackathon.util.mail;

import javax.mail.MessagingException;

import br.com.hackathon.entity.Usuario;
import br.com.hackathon.lib.enuns.TipoEmails;
import br.com.hackathon.util.MailException;

//Tutorial: http://claudioweiler.blogspot.com.br/2012/06/enviando-e-mail-com-java.html
public class EmailService extends Email{
	
	/**
	 * 
	 * @param tipoEmails, serve para saber qual tipo de convers�o que ter� de fazer
	 * @param object, pode ser um Usu�rio ou...
	 * @throws MessagingException 
	 * @throws Exception
	 */
	public void sendEmailByAdministrador(TipoEmails tipoEmails, Object object) throws MessagingException, MailException {
		/*if(tipoEmails == TipoEmails.NOVO_USUARIO_NOVA_PROPRIEDADE) { //Propriet�rio novo
			TemplatePrimeiroUsuario primeiroUsuario = new TemplatePrimeiroUsuario();
			primeiroUsuario.configurar(getSessionAdministrador() , ((Usuario)object).getEmail(), object, null);
			primeiroUsuario.enviarEmail();
		} else if(tipoEmails == TipoEmails.NOVO_USUARIO_PROPRIEDADE_EXISTENTE) { //Funcion�rio novo
			TemplatePrimeiroUsuarioPropriedadeExistente primeiroUsuarioPropriedadeExistente = new TemplatePrimeiroUsuarioPropriedadeExistente();
			primeiroUsuarioPropriedadeExistente.configurar(getSessionAdministrador() , ((Usuario)object).getEmail(), object, propriedade);
			primeiroUsuarioPropriedadeExistente.enviarEmail();
		} else if(tipoEmails == TipoEmails.USUARIO_EXISTENTE_PROPRIEDADE_EXISTE) { //Funcion�rio com login existente
			TemplateUsuarioExistentePropriedadeExistente usuarioExistentePropriedadeExistente = new TemplateUsuarioExistentePropriedadeExistente();
			usuarioExistentePropriedadeExistente.configurar(getSessionAdministrador() , ((Usuario)object).getEmail(), object, propriedade);
			usuarioExistentePropriedadeExistente.enviarEmail();
		} else */if(tipoEmails == TipoEmails.RECUPERAR_SENHA) {
			TemplateRecuperarSenha recuperarSenha = new TemplateRecuperarSenha();
			recuperarSenha.configurar(getSessionAdministrador() , ((Usuario)object).getEmail(), object);
			recuperarSenha.enviarEmail();
		}
	}
}