package br.com.hackathon.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.validator.constraints.Email;

import br.com.hackathon.util.FacesUtil;
import br.com.hackathon.util.MensagemUtil;

@Named
@ViewScoped
public class LoginBean implements Serializable {
	
	private static final long serialVersionUID = 6624584046961968030L;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private HttpServletRequest request;
	
	@Inject
	private HttpServletResponse response;
	
	@Email
	private String email;

	public void preRender(String message) {
		if(message != null && !"".equals(message)) {
			if("Bad credentials".equals(message)) {
				FacesUtil.addErrorMessage(MensagemUtil.getMensagem("erro_usuario_ou_senha_invalido"));
			} else {
				FacesUtil.addErrorMessage(MensagemUtil.getMensagem(message));
			}
		}
	}
	
	public void login() throws ServletException, IOException {
		/*RequestDispatcher dispatcher = request.getRequestDispatcher(Link.LOGIN.getPagina());
		dispatcher.forward(request, response);
		
		facesContext.responseComplete();*/
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}