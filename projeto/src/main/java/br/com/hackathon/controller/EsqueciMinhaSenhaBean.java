package br.com.hackathon.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import br.com.hackathon.lib.enuns.Link;
import br.com.hackathon.negocio.UsuarioRN;
import br.com.hackathon.util.FacesUtil;

@Named
@ViewScoped
public class EsqueciMinhaSenhaBean implements Serializable{
	private static final long serialVersionUID = -4463512733146038672L;

	private String email;
	
	@Inject
	private UsuarioRN usuarioRN;
	
	@PostConstruct
	public void init() {
		email = null;
	}
	
	public String enviarEmail() {
		try {
			usuarioRN.reenviarEmailParaRecuperarSenha(email);
			return Link.LINK_WEB.getDescricao() + Link.LOGIN.getDescricao() + "?faces-redirect=true";
		} catch (Exception e) {
			FacesUtil.addErrorMessage(e.getMessage());
			return "";
		}
	}

	@NotNull @NotBlank @NotEmpty
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
