function configurarMetrosQuadrados() {
			$(".metrosQuadrados").maskMoney({ decimal: ",", thousands: ".", allowZero: true, precision: 0, allowNegative: false});
}

function configurarHectare() {
	$(".hectare").maskMoney({ decimal: ",", thousands: ".", allowZero: true, precision: 3, allowNegative: false});
}

$(document).ready(function() {
	configurarHectare();
});

/* Máscaras ER */
function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
	v=v.replace(/\D/g,""); //Remove tudo o que não é dígito
	v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
	v=v.replace(/(\d)(\d{4})$/,"$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
	
	return v;
}
	
function getByclass( el ){
	return document.getElementsByClassName( el );
}

window.onload = function(){
	var elems = document.getElementsByClassName( 'mascaraTelefone' )
	
	for (var i = 0; i < elems.length; i++ ) {
		elems[i].onkeypress = function(){
			$(this).attr('maxlength', 15);
			mascara( this, mtel );
		}
	}
}

/* Máscaras CPF CNPJ */
function str_replaceCpfCnpj(busca,subs,valor){
    var ret=valor;
    var pos=ret.indexOf(busca);
    while(pos!=-1){
        ret=ret.substring(0,pos)+subs+ret.substring(pos+busca.length,ret.length);
        pos=ret.indexOf(busca);
    }
    return ret;
}
 
function mascaraCpfCnpj(valor,masc){
    var res=valor,mas=str_replaceCpfCnpj("?","",str_replaceCpfCnpj("9","",masc));
	
    for(var i=0;i<mas.length;i++){
	    res=str_replaceCpfCnpj(mas.charAt(i),"",res);
	mas=str_replaceCpfCnpj(mas.charAt(i),"",mas);
	}
	var ret="";
	
	for(var i=0;i<masc.length&&res!="";i++){ switch(masc.charAt(i)){ case"?": ret+=res.charAt(0); res=res.substring(1,res.length); break; case"9": while(res!=""&&(res.charCodeAt(0)>57||res.charCodeAt(0)<48))res=res.substring(1,res.length);
	        if(res!=""){
	                    ret+=res.charAt(0);
	                    res=res.substring(1,res.length);
	                }
	                break;
	            default:
	                ret+=masc.charAt(i);
	        }
	    }
	    return ret;
}
 
$(document).ready(function(){
         
    $(".mascaraCpfCnpj").keyup(function(){
     
    if($(this).val().length <= 14) 
        $(this).val( mascaraCpfCnpj($(this).val(), "999.999.999-99") ); 
    else
        $(this).val( mascaraCpfCnpj($(this).val(), "999.999.999/9999-99") ); 
         
    })
   
    $(".mascaraCpfCnpj").bind("focus blur", function() {
    	if( !($(this).val().length == 14 || $(this).val().length == 19) )
    		$(this).val(""); 
    })     
});

$(".moneyMask").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true});
$(".quantidadeMask").maskMoney({thousands:'.', decimal:','});